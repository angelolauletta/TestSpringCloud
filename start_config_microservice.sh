#!/bin/bash

CONFIG_REPO_URI=https://github.com/angelolauletta/TestSpringCloud.git
CONFIG_REPO_PATH=config-repo
CONFIG_SERVER_PORT=11001
DEBUG="-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=6005,suspend=n"

function wait_till_started {
	until [ "`curl --silent --show-error --connect-timeout 1 http://localhost:$1/health | grep 'UP'`" != "" ];
	do
	  echo "sleeping for 10 seconds..."
	  sleep 10
	done
}

function update_in_config_repo {
	echo "prop.date=`date`" > microservice.properties
	git add microservice.properties
	git status
	git commit -m "Auto commit"
	git push origin master
}
 
printf "\n\nStarting the config-server...\n\n"
java -Dport=$CONFIG_SERVER_PORT -Dconfig.repo.uri=$CONFIG_REPO_URI -Dconfig.repo.path=$CONFIG_REPO_PATH -jar config-server/target/config-server-0.0.1-SNAPSHOT.jar &
wait_till_started   $CONFIG_SERVER_PORT

printf "\n\nStarting the microservice...\n\n"
java $DEBUG -Dconfig.server.uri=http://localhost:$CONFIG_SERVER_PORT -Dport=14001 -jar microservice/target/microservice-0.0.1-SNAPSHOT.jar &
wait_till_started   14001


